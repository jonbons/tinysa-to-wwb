var stepped = 0, rowCount = 0, errorCount = 0, firstError;
var start, end;
var firstRun = true;

document.addEventListener("DOMContentLoaded", function(event) { 
    $('#submit').click(function()
	{
		stepped = 0;
		rowCount = 0;
		errorCount = 0;
		firstError = undefined;

		var config = buildConfig();
		var input = $('#input').val();

		if (!firstRun)
			console.log("--------------------------------------------------");
		else
			firstRun = false;

        if (!$('#files')[0].files.length)
        {
            alert("Please choose a file to parse.");
        }
        
        $('#files').parse({
            config: config
        });
	});
});

function printStats(msg)
{
	if (msg)
		console.log(msg);
	console.log("       Time:", (end-start || "(Unknown; your browser does not support the Performance API)"), "ms");
	console.log("  Row count:", rowCount);
	if (stepped)
		console.log("    Stepped:", stepped);
	console.log("     Errors:", errorCount);
	if (errorCount)
		console.log("First error:", firstError);
}

function buildConfig()
{
	return {
		delimiter: ';',
		header: false,
		dynamicTyping: true,
		skipEmptyLines: true,
		complete: completeFn,
		error: errorFn
	};
}

function stepFn(results, parser)
{
	stepped++;
	if (results)
	{
		if (results.data)
			rowCount += results.data.length;
		if (results.errors)
		{
			errorCount += results.errors.length;
			firstError = firstError || results.errors[0];
		}
	}
}

function completeFn(results)
{
	end = now();

	if (results && results.errors)
	{
		if (results.errors)
		{
			errorCount = results.errors.length;
			firstError = results.errors[0];
		}
		if (results.data && results.data.length > 0)
			rowCount = results.data.length;
	}

	printStats("Parse complete");
    parseResults(results);
}

function parseResults(results)
{
    results.data.forEach((r) => {
        r[0] = parseFloat((r[0] / 1000000).toFixed(3));
        r[1] = Math.round(r[1]);
    });
    console.log(results.data);
}

function errorFn(err, file)
{
	end = now();
	console.log("ERROR:", err, file);
	enableButton();
}

function now()
{
	return typeof window.performance !== 'undefined'
			? window.performance.now()
			: 0;
}